<?php
class ModelToolQuickRegister extends Model {
	public function addCustomer($data) {
  $token = md5(time(1));
  $this->db->query("INSERT INTO " . DB_PREFIX . "customer SET store_id = '" . (int)$this->config->get('config_store_id') . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '', email = '" . $this->db->escape($data['email']) . "', telephone = '" . $this->db->escape($data['phone']) . "', fax = '', password = '" . $this->db->escape(md5($data['password'])) . "', newsletter = '" . (isset($data['newsletter']) ? (int)$data['newsletter'] : 0) . "', customer_group_id = '" . (int)$this->config->get('config_registred_group_id') . "', status = '1', date_added = NOW(), token='".$token."'");
      	
		$customer_id = $this->db->getLastId();
			
		if (!$this->config->get('config_customer_approval')) {
			$this->db->query("UPDATE " . DB_PREFIX . "customer SET approved = '1' WHERE customer_id = '" . (int)$customer_id . "'");
		}	
		
		$this->language->load('mail/quickregister');
		
		$subject = sprintf($this->language->get('text_subject'), $this->config->get('config_name'));
    $subject = "Благодарим за регистрацию!";
		
		$message = sprintf($this->language->get('text_welcome'), $this->config->get('config_name')) . "\n\n";
		
		if (!$this->config->get('config_customer_approval')) {
			$message .= $this->language->get('text_login') . "\n";
		} else {
			$message .= $this->language->get('text_approval') . "\n";
		}
		
		$message .= $this->url->link('account/login', '', 'SSL') . "\n\n";
		$message .= $this->language->get('text_email') . $data['email'] . "\n\n";
		$message .= $this->language->get('text_password') . $data['password'] . "\n\n";
		$message .= $this->language->get('text_services') . "\n\n";
		$message .= $this->language->get('text_thanks') . "\n";
		$message .= $this->config->get('config_name');

    $message = "Здравствуйте, ".$data['firstname']."!\n
                Вы зарегестрировались в интернет-Магазине Сайма.\n
                (Напольные покрытия, Двери, Потолки ...)\n
                Для входа в личный кабинет и просмотра истории покупок, используйте эту ссылку:\n
                http://www.magazinsaima.ru/index.php?route=account/login&token=".$token."\n
                С уважением, коллектив Магазин Сайма.\n
                8 (812) 611-0399, 8 (911) 123-2758\n
                ----------------------------------------------------\n
                Это письмо было отправлено автоматически с сайта http://www.magazinsaima.ru";


    $mail = new Mail();
		$mail->protocol = $this->config->get('config_mail_protocol');
		$mail->parameter = $this->config->get('config_mail_parameter');
		$mail->hostname = $this->config->get('config_smtp_host');
		$mail->username = $this->config->get('config_smtp_username');
		$mail->password = $this->config->get('config_smtp_password');
		$mail->port = $this->config->get('config_smtp_port');
		$mail->timeout = $this->config->get('config_smtp_timeout');				
		$mail->setTo($data['email']);
		$mail->setFrom($this->config->get('config_email'));
		$mail->setSender($this->config->get('config_name'));
		$mail->setSubject($subject);
		$mail->setText($message);
		$mail->send();
		
		// Send to main admin email if new account email is enabled
		if ($this->config->get('config_account_mail')) {
			$mail->setTo($this->config->get('config_email'));
			$mail->send();
			
			// Send to additional alert emails if new account email is enabled
			$emails = explode(',', $this->config->get('config_alert_emails'));
			
			foreach ($emails as $email) {
				if (strlen($email) > 0 && preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $email)) {
					$mail->setTo($email);
					$mail->send();
				}
			}
		}
	}
}
?>