<?php
class ModelTotalOptGroupDiscount extends Model {
	public function getTotal(&$total_data, &$total, &$taxes) {
		$this->load->language('total/opt_group_discount');
		$sub_total = 0;

        $cart_data = $this->cart->getProducts();
        $this->load->model('catalog/product');
        foreach($cart_data as $product){
            $discount_status = $this->model_catalog_product->discount_status($product['product_id'],$product['quantity']);
            if(!$discount_status){
                $sub_total += $product['total'];
            }
        }

		// Если меньше 1 000, то скидка не применяется
        $persent = 0;
        if($sub_total < 5000){
            return false;
        }elseif($sub_total > 10000){
            $persent = 5;
        }else{
            $persent = 3;
        }

		$discount =  (float) ($sub_total * $persent / 100);
        $total -= $discount;
		
			  $total_data[] = array( 
					'code'	   => 'opt_group_discount',
					'title'	  => $this->language->get('text_discount') ."(" . $persent . "%)",
					'text'	   => "-".$this->currency->format($discount),
					'value'	  => $discount,
					'sort_order' => $this->config->get('discount_sort_order')
				);		
		
	}
	

}
?>