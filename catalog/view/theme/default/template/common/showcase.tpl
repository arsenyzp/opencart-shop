<div class="box">
  <div class="box-heading">Новости</div>
	<div class="box-content">
  <?php
    if (count($news_m)>0) {
	?>
	<ul>
	<?php
	foreach ($news_m as $news) {
		if($news['catalogcategory'] > 0){
			if($current_category_id != $news['catalogcategory'])
				continue;
		}
  ?>
    <li>
      <span style="display: block;font-size: 13px; color: #a3bad9"><?php echo $news['date_added']; ?></span>
      <a style="font-size: 13px;" href="index.php?route=record/blog&record_id=<?php echo $news['record_id']; ?>"><?php echo $news['name']; ?></a>
    </li>
  <?php
	  }
	?>
	</ul>
	<?php
  }
  ?>
    <span><a href="/first_blog/">Все новости</a></span>
	</div>
</div>

<div class="box">
  <div class="box-heading">Статьи</div>
  <div class="box-content">
    <?php
    if (count($news_b)>0) {
    ?>
    <ul>
      <?php
	foreach ($news_b as $news) {
		if($news['catalogcategory'] > 0){
      if($current_category_id != $news['catalogcategory'])
      continue;
      }
      ?>
      <li>
        <span style="display: block;font-size: 13px; color: #a3bad9"><?php echo $news['date_added']; ?></span>
        <a style="font-size: 13px;" href="index.php?route=record/blog&record_id=<?php echo $news['record_id']; ?>"><?php echo $news['name']; ?></a>
      </li>
      <?php
	  }
	?>
    </ul>
    <?php
  }
  ?>
    <span><a href="/index.php?route=record/blog&blog_id=2">Все статьи</a></span>
  </div>
</div>