<?php //var_dump($manufactureres)?>
<div class="box">
  <div class="box-heading"><?php echo $heading_title; ?></div>
  <div class="box-content">
    <div class="box-category">
      <ul>
        <?php foreach ($manufactureres as $manufacturer) { ?>
        <li>
			<a href="<?php echo $manufacturer['href']; ?>"><?php echo $manufacturer['name']; ?></a>
        </li>
        <?php } ?>
      </ul>
    </div>
  </div>
</div>
