<div class="box">
  <div class="box-heading"><?php echo $box_title; ?></div>
  <div class="box-content">
  <div class="links" <?php if ($layout!='vertical'){echo 'align="center"';} ?>>
    <?php
	foreach($links as $link){
		echo '<a style="margin-right:2em;line-height:2em; text-decoration:none;" href="'.$link['url'].'" target="_blank">'.$link['text'].'</a>';
		if ($layout=='vertical'){echo '<br/>';}
	}
	?>
	</div>
  </div>
</div>
