    <?php foreach ($products as $product) { ?>
    <div>
      <?php if ($product['thumb']) { ?>
      <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" /></a></div>
      <?php } ?>
      <div class="name">
        <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
        <div class="ups"><?php echo $product['upc']; ?></div>
      </div>
      <div class="description">
        <?php echo $product['description']; ?>
        <br />
        <a href="<?php echo $product['href']; ?>" class="button">Подробнее...</a>
      </div>
      <?php if ($product['price']) { ?>
      <div class="price">
        <?php if (!$product['special']) { ?>
         <div id="currentPrice_<?php echo $product['product_id']; ?>"><?php echo $product['price']; ?></div>
        <?php } else { ?>
        <span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span>
        <?php } ?>
        <?php if ($product['tax']) { ?>
        <br />
        <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
        <?php } ?>

 <?php if( $product['discount_precent'] > 0){ ?>
      <div>
        <div>Скидка при заказе</div>
        <div class="ups"><?php echo $product['discount_precent'] ; ?> %</div>
        <input type="button" value="Пересчитать со скидкой" onclick="getPriceDiscount2(<?php echo $product['discount_precent'] ; ?>, '<?php echo $product['price'] ; ?>', '<?php echo $product['product_id']; ?>');" />
      </div>
      <?php } ?>


      </div>
      <?php } ?>
      <?php if ($product['rating']) { ?>
      <div class="rating"><img src="catalog/view/theme/default/image/stars-<?php echo $product['rating']; ?>.png" alt="<?php echo $product['reviews']; ?>" /></div>
      <?php } ?>
      <div class="cart">
        <input type="button" value="<?php echo $button_cart; ?>" onclick="addToCart('<?php echo $product['product_id']; ?>');" class="button" />
      </div>
      <div class="wishlist"><a onclick="addToWishList('<?php echo $product['product_id']; ?>');"><?php echo $button_wishlist; ?></a></div>
      <div class="compare"><a onclick="addToCompare('<?php echo $product['product_id']; ?>');"><?php echo $button_compare; ?></a></div>
    </div>
    <?php } ?>