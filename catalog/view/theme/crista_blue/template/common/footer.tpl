<div id="footer">
  <div class="column">
    <h3><?php echo $text_information; ?></h3>
    <ul>
      <?php foreach ($informations as $information) { ?>
      <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
      <?php } ?>
    </ul>
  </div>
  <div class="column">
    <h3><?php echo $text_service; ?></h3>
    <ul>
      <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
      <li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
      <li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
    </ul>
  </div>
  <div class="column">
    <h3><?php echo $text_extra; ?></h3>
    <ul>
      <li><a href="<?php echo $manufacturer; ?>"><?php echo $text_manufacturer; ?></a></li>
      <li><a href="<?php echo $voucher; ?>"><?php echo $text_voucher; ?></a></li>
      <li><a href="<?php echo $affiliate; ?>"><?php echo $text_affiliate; ?></a></li>
      <li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
    </ul>
  </div>
  <div class="column">
    <h3><?php echo $text_account; ?></h3>
    <ul>
      <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
      <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
      <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
      <li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
    </ul>
  </div>
</div>
<div>
<?php echo $footer_record; ?>
</div>
<?php
    global $sape;
    if (!defined('_SAPE_USER')){
        define('_SAPE_USER', 'c029f553911024c9f90e0dcbcbaf9215');
    }
    require_once(realpath($_SERVER['DOCUMENT_ROOT'].'/'._SAPE_USER.'/sape.php'));
    $sape = new SAPE_client();
?>

<!-- 
OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donatation.
Please donate via PayPal to donate@opencart.com
//-->
<div id="powered">
</div>
<!-- 
OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donatation.
Please donate via PayPal to donate@opencart.com
//-->


</div>
<script type="text/javascript">
//*
// Укорачиваем слишком длинные "крошки"
//*

    // Максимальное кол-во символов в крошках

    var max_bread_text_count = 100;

    // Получаем кол-во символов в крошках
    var bread_text_count = get_bread_text_length();
    /*console.log(bread_text_count);*/


    var jj = 20;  // Ограничение на кол-во интераций
    var need_slice = false; // Нужно ли урезать
    var slice_marker_add = false;  // Вставленны ли 3 точки
    var slice_num; // Сколько обрезать символов
    var sliced_bread_text; // Обрезанный текст крошки
        // Интеративно уменьшаем крошки до приемлимой длинны
        while(max_bread_text_count <  bread_text_count && jj > 1){
            need_slice = true; // Пришлось обрезать
            // Проверяем удалять крошку целиком или можно только частично обрезать
            if(bread_text_count - $(".breadcrumb a:first").text().length + 3 >  max_bread_text_count){
                $(".breadcrumb a:first").remove(); // Удаляем крошку целиком
            }else{
                slice_num = bread_text_count - max_bread_text_count  + 3; // вычисляем сколько отрезать
                // обрезаем
                sliced_bread_text = "..." + $(".breadcrumb a:first").text().substr(slice_num);
                $(".breadcrumb a:first").text(sliced_bread_text);

                slice_marker_add = true; // три точки уже вставленны
            }
            // обновляем кол-во оставшихся символов
            bread_text_count = get_bread_text_length();

            jj = jj - 1; // уменьшаем ограниченить интереаций

        }

        // Заного формируем стоку с крошками
        var bread_new_html = "";
        $(".breadcrumb a").each(function(){
            bread_new_html += $("<p>").append($(this).clone()).html();
        });
        $(".breadcrumb").html(bread_new_html);
        $(".breadcrumb a").slice(0, -1).after(" » ");

        // Если пришлось обрезать, но три точки ещё не вставлены, то вставляем их
        if(slice_marker_add == false && need_slice == true){
            $(".breadcrumb a:first").before("... » ");
        }

        $(".breadcrumb").css("visibility", "visible"); // Отображаем крошки



    // Функция вычисляющая кол-во символов в крошках
    function get_bread_text_length(){
        var bread_text_count_l = 0;
        $(".breadcrumb a").each(function(){
            bread_text_count_l += $(this).text().length + 4;
        });
        return bread_text_count_l;
    }

 //    var compate_list = eval('<?php echo  json_encode( array_values( $this->session->data['compare'] ) ); ?>');
  //  $.each(compate_list, function(i, val){
   //     $("#product_" + val + " .compare a").css("fontWeight", "bold").text('Сравнить').attr("onClick", false).bind("click", function(){

    //        document.location = "/index.php?route=product/compare";
    //    });

   // })

</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-35229831-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter19605367 = new Ya.Metrika({id:19605367,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/19605367" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<script type="text/javascript" src="/catalog/view/javascript/k.js"></script>
<!-- Start SiteHeart code -->
<script>
(function(){
var widget_id = 675322;
_shcp =[{widget_id : widget_id}];
var lang =(navigator.language || navigator.systemLanguage 
|| navigator.userLanguage ||"en")
.substr(0,2).toLowerCase();
var url ="widget.siteheart.com/widget/sh/"+ widget_id +"/"+ lang +"/widget.js";
var hcc = document.createElement("script");
hcc.type ="text/javascript";
hcc.async =true;
hcc.src =("https:"== document.location.protocol ?"https":"http")
+"://"+ url;
var s = document.getElementsByTagName("script")[0];
s.parentNode.insertBefore(hcc, s.nextSibling);
})();
</script>
<!-- End SiteHeart code -->
<style>
  .sp a{
    color: #000000 !important;
    font-size: 10px;
  }
  .sp {
    color: #000000 !important;
    font-size: 10px;
  }
</style>
<center class="sp">
  <?php
    global $sape;
    echo iconv("windows-1251", "UTF-8", $sape->return_links());
  ?>
</center>
</body></html>
