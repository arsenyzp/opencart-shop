<?php
// Text
$_['text_refine']       = 'Выберите альбом';
$_['heading_title']     = 'Все фото';
$_['text_product']      = 'Товаров';
$_['text_comments']     = 'Комментариев';
$_['button_more']       = 'Подробнее';
$_['text_error']        = 'Фото в данном альбоме не найдены!';
$_['text_empty']        = 'В этом альбоме нет фото.';
?>
