<?php
// Heading
$_['heading_title']     = 'Поиск фото';
 
// Text
$_['text_search']       = 'Фото, соответствующие критериям поиска';
$_['text_keyword']      = 'Поиск...';
$_['text_category']     = 'Все альбомы';
$_['text_sub_category'] = 'Искать в альбомах';
$_['text_critea']       = 'Критерии поиска:';
$_['text_empty']        = 'Нет фото, которые соответствуют критериям поиска.';
$_['text_default']      = 'По умолчанию';
$_['text_comments']     = 'Комментариев';
$_['button_more']       = 'Подробнее';

// Entry
$_['entry_search']      = 'Поиск:';
$_['entry_description'] = 'Искать в описаниях фото ';
?>
