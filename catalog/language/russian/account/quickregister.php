<?php
// Heading 
$_['heading_title']        = 'Быстрая регистрация';

// Text
$_['text_account']         = 'Личный Кабинет';
$_['text_register']        = 'Быстрая регистрация';
$_['text_account_already'] = 'Если Вы уже зарегистрированы, перейдите на страницу <a href="%s">входа в систему</a>.';
$_['text_your_details']    = 'Основные данные';
$_['text_agree']           = 'Я прочитал и принимаю <a class="fancybox" href="%s" alt="%s"><b>%s</b></a>';

// Entry
$_['entry_firstname']      = 'Ваше имя:';
$_['entry_lastname']       = 'Фамилия:';
$_['entry_email']          = 'E-Mail:';
$_['entry_newsletter']     = 'Получать рассылку от магазина:';
$_['entry_password']       = 'Придумайте пароль:';

// Error
$_['error_exists']         = 'Этот E-Mail уже зарегистрирован!';
$_['error_firstname']      = 'Имя должно содержать от 1 до 32 символов!';
$_['error_email']          = 'E-Mail введён неправильно!';
$_['error_password']       = 'В пароле должно быть от 4 до 20 символов!';
$_['error_agree']          = 'Для совершения покупок Вы должны быть согласны с документом %s!';
?>