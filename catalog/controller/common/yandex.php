<?php

class ControllerCommonYandex extends Controller {

	protected function index() {
		$this->id = 'yandex';

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/ template/common/yandex.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/common/yandex.tpl';
		} else {
			$this->template = 'default/template/common/yandex.tpl';
		}

		$headers = array(
		    'Host: 92.113.121.60', 
		    'Accept: */*', 
		    'Authorization: 215555b5357044309904ce4ea1697c66', 
		);

		$url = "https://api.content.market.yandex.ru/v1/shop/21096992/opinion.xml?sort=rank&count=2"; 
		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL,$url); // set url to post to 
		//curl_setopt($ch, CURLOPT_FAILONERROR, 1); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);// allow redirects 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); // return into a variable 
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		$content = curl_exec($ch); // run the whole process 
		curl_close($ch);   

		$this->data['rewiews'] = $content;

		$this->render();
	}
}