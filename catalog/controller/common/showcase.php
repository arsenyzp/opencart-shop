<?php

class ControllerCommonShowcase extends Controller {

	protected function index() {
		$this->id = 'showcase';

		//$this->load->language('news/article');
		
		$this->load->model('catalog/record');
		$this->load->model('catalog/category');
    $this->load->model('catalog/blog');

    $parts = explode('_', (string)$this->request->get['path']);

    $category_id = (int)array_pop($parts);

		$news_info = $this->model_catalog_record->getRecords(array('category_id'=>$category_id, 'filter_blog_id'=>1, 'limit'=>4));

		$this->data['news_m'] = $news_info;

    //$news_info = $this->model_catalog_record->getTT(2, $category_id);
    $news_info = $this->model_catalog_record->getRecords(array('category_id'=>$category_id, 'filter_blog_id'=>2, 'limit'=>4));
    $this->data['news_b'] = $news_info;



		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/ template/common/showcase.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/common/showcase.tpl';
		} else {
			$this->template = 'default/template/common/showcase.tpl';
		}

		$parts = array(0);
		if (isset($this->request->get['path'])) {
		 $parts = explode('_', (string)$this->request->get['path']);
		} elseif (isset($this->request->get['product_id'])) { 
		 if ($rows = $this->model_catalog_category->getCategoriesByProductId($this->request->get['product_id'])) {
		 $parts = array_values($rows[0]);
		 }
		}
		$this->data['current_category_id'] = (int)array_pop($parts);
		$this->render();
	}
}
