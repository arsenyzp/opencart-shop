<?php
class ControllerModuleLinks extends Controller {
	protected function index($setting) {

		$this->language->load('module/links');
 		
      	$this->data['heading_title'] = $this->language->get('heading_title');
				
		$links = $setting;
				
		if ($links['position']=='content_top' || $links['position']=='content_bottom'){
			$this->data['layout'] = 'horizontal';
		}
		else
		{
			$this->data['layout'] = 'vertical';
		}
		
		$this->data['box_title'] = $links['box_title'];
		$this->data['links'] = array();
	    
		foreach ( $links['items'] as $item){
		if ($item['linkText']!='' && $item['linkUrl'] != '' )
		{
		$this->data['links'][] = array(
				'text' => $item['linkText'],
				'url' => $item['linkUrl'],
				);
			}
		}
		
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/links.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/links.tpl';
		} else {
			$this->template = 'default/template/module/links.tpl';
		}

		$this->render();
	}
}
?>