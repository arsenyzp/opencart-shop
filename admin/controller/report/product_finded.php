<?php
class ControllerReportProductFinded extends Controller {
	public function index() {     
		$this->load->language('report/product_finded');

		$this->document->setTitle($this->language->get('heading_title'));
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';
				
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
						
		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('report/product_finded', 'token=' . $this->session->data['token'] , 'SSL'),
      		'separator' => ' :: '
   		);		
		
		$this->load->model('report/product');
		
		$data = array(
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);
				
		$product_viewed_total = $this->model_report_product->getTotalProductsFinded($data);
		

		$this->data['products'] = array();
		
		$results = $this->model_report_product->getProductsFinded($data);
		
		foreach ($results as $result) {

			$this->data['products'][] = array(
				'name'    => $result['text'],
				'model'   => $result['text'],
				'viewed'  => $result['count'],
				'percent' => round($result['avg_finded'], 2),
                'more'    => $this->url->link('report/product_finded/more', 'token=' . $this->session->data['token'] . '&text=' . urlencode($result['text']) , 'SSL')
			);
		}
 		
		$this->data['heading_title'] = $this->language->get('heading_title');
		 
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		
		$this->data['column_name'] = $this->language->get('column_name');
		$this->data['column_model'] = $this->language->get('column_model');
		$this->data['column_viewed'] = $this->language->get('column_viewed');
		$this->data['column_percent'] = $this->language->get('column_percent');
		
		$this->data['button_reset'] = $this->language->get('button_reset');

		$url = '';		
				
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
				
		$this->data['reset'] = $this->url->link('report/product_finded/reset', 'token=' . $this->session->data['token'] . $url, 'SSL');

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
						
		$pagination = new Pagination();
		$pagination->total = $product_viewed_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('report/product_finded', 'token=' . $this->session->data['token'] . '&page={page}', 'SSL');
			
		$this->data['pagination'] = $pagination->render();
				 
		$this->template = 'report/product_finded.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}

    public function more() {
        $this->load->language('report/product_finded');

        $this->document->setTitle($this->language->get('heading_title_more')  . ' «' . $this->request->get['text'] . '»' );

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $url = '';

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('report/product_finded', 'token=' . $this->session->data['token'] , 'SSL'),
            'separator' => ' :: '
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title_more')  . ' «' . $this->request->get['text'] . '»',
            'href'      => $this->url->link('report/product_finded/more', 'token=' . $this->session->data['token'] , 'SSL'),
            'separator' => ' :: '
        );

        $this->load->model('report/product');

        $data = array(
            'start' => ($page - 1) * $this->config->get('config_admin_limit'),
            'limit' => $this->config->get('config_admin_limit'),
            'text'  => $this->request->get['text']
        );

        $product_viewed_total = $this->model_report_product->getTotalProductsFindedMore($data);


        $this->data['products'] = array();

        $results = $this->model_report_product->getProductsFindedMore($data);

        foreach ($results as $result) {

            $this->data['products'][] = array(
                'date'    => $result['date'],
                'finded_num'   => $result['finded_num'],
                'more' => $this->url->link('report/product_finded/ajax_finded_products', 'token=' . $this->session->data['token'] . '&search_id=' . $result['search_id'], 'SSL')
            );
        }

        $this->data['heading_title'] = $this->language->get('heading_title_more') . ' «' . $this->request->get['text'] . '»';

        $this->data['text_no_results'] = $this->language->get('text_no_results');
        $this->data['text_finded_list'] = $this->language->get('text_finded_list');

        $this->data['column_more_date'] = $this->language->get('column_more_date');
        $this->data['column_more_time'] = $this->language->get('column_more_time');
        $this->data['column_finded_num'] = $this->language->get('column_finded_num');
        $this->data['column_finded_prods'] = $this->language->get('column_finded_prods');
        $this->data['column_viewed'] = $this->language->get('column_viewed');
        $this->data['column_percent'] = $this->language->get('column_percent');

        $this->data['button_reset'] = $this->language->get('button_reset');

        $url = '';

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        if (isset($this->session->data['success'])) {
            $this->data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $this->data['success'] = '';
        }

        $pagination = new Pagination();
        $pagination->total = $product_viewed_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_admin_limit');
        $pagination->text = $this->language->get('text_pagination');
        $pagination->url = $this->url->link('report/product_finded/more', 'token=' . $this->session->data['token'] . '&page={page}' . '&text=' . urlencode($this->request->get['text']) , 'SSL');

        $this->data['pagination'] = $pagination->render();

        $this->template = 'report/product_finded_more.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );

        $this->response->setOutput($this->render());
    }
    public function ajax_finded_products() {
        $search_id = $this->request->get['search_id'];
        $this->load->model('report/product');
        $results = $this->model_report_product->getFindedList($search_id);

        $this->data['products'] = $results;

        $this->template = 'report/product_finded_ajax.tpl';
        $this->response->setOutput($this->render());
    }

    public function reset() {
		$this->load->language('report/product_finded');
		
		$this->load->model('report/product');
		
		$this->model_report_product->reset_finded();
		
		$this->session->data['success'] = $this->language->get('text_success');
		
		$this->redirect($this->url->link('report/product_viewed', 'token=' . $this->session->data['token'], 'SSL'));
	}
}
?>