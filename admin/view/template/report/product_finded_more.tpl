<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/report.png" alt="" /> <?php echo $heading_title; ?></h1>

    </div>
    <div class="content">
      <table class="list">
        <thead>
          <tr>
            <td class="left"><?php echo $column_more_date; ?></td>
            <td class="left"><?php echo $column_more_time; ?></td>
            <td class="right"><?php echo $column_finded_num; ?></td>
            <td class="right"><?php echo $column_finded_prods; ?></td>
          </tr>
        </thead>
        <tbody>
          <?php if ($products) { ?>
          <?php foreach ($products as $product) { ?>
          <tr>
            <td class="left"><?php echo date('d.m.Y', strtotime($product['date'])); ?></td>
            <td class="left"><?php echo date('G:i:s', strtotime($product['date'])); ?></td>
            <td class="right"><?php echo $product['finded_num']; ?></td>
            <td class="right"><?php if ($product['finded_num']){ ?><a class="ajax" href="<?php echo $product['more']; ?>"><?php echo $text_finded_list; ?></a><?php } ?></td>

          </tr>
          <?php } ?>
          <?php } else { ?>
          <tr>
            <td class="center" colspan="4"><?php echo $text_no_results; ?></td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
      <div class="pagination"><?php echo $pagination; ?></div>
    </div>
  </div>
</div>
<script type="text/javascript">
    $(function (){
        $('a.ajax').click(function() {
            var url = this.href;
            // show a spinner or something via css
            var dialog = $('<div style="display:none" class="loading"></div>').appendTo('body');
            // open the dialog
            dialog.dialog({
                // add a close listener to prevent adding multiple divs to the document
                width: 700,
                resizable: false,
                position: "center",
                close: function(event, ui) {
                    // remove div with all data and events
                    dialog.remove();
                },
                modal: true,
                title: "<?php echo $text_finded_list; ?>"
            });
            // load remote content
            dialog.load(
                    url,
                    {}, // omit this param object to issue a GET request instead a POST request, otherwise you may provide post parameters within the object
                    function (responseText, textStatus, XMLHttpRequest) {
                        // remove the loading class
                        dialog.removeClass('loading');
                    }
            );
            //prevent the browser to follow the link
            return false;
        });
    });
</script>
<?php echo $footer; ?>