<?php
// Heading
$_['heading_title']  = 'Отчет по запросам в поиске';
$_['heading_title_more']  = 'Подробно о запросе';

// Text
$_['text_success']   = 'Список искомых товаров очищен!';
$_['text_finded_list']   = 'Список найденного';

// Column
$_['column_name']    = 'Запрос';
$_['column_model']   = 'Подробно';
$_['column_viewed']  = 'Совершено запросов';
$_['column_percent'] = 'Среднее количество найденных товаров';

$_['column_more_date']  = 'Дата запроса';
$_['column_more_time']  = 'Время запроса';
$_['column_finded_num']   = 'Найдено товаров';
$_['column_finded_prods']   = 'Найденые товары';


?>