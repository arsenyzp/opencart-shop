<?php
/******************************************************
 * @package Pav Megamenu module for Opencart 1.5.x
 * @version 1.0
 * @author http://www.pavothemes.com
 * @copyright	Copyright (C) Feb 2013 PavoThemes.com <@emai:pavothemes@gmail.com>.All rights reserved.
 * @license		GNU General Public License version 2
*******************************************************/

// Heading
$_['heading_title']       = '<b>Управление меню</b>';

// Text
$_['text_treemenu']     = 'Управление деревом меню';
 $_['text_menu_assignment'] = 'Настройки модуля';
$_['text_module']         = 'Дополнения';
$_['text_success']        = 'Вы изменили настройки модуля!';
$_['text_content_top']    = 'Content Top';
$_['text_content_bottom'] = 'Content Bottom';
$_['text_column_left']    = 'Column Left';
$_['text_column_right']   = 'Column Right';
$_['text_mainmenu']   = 'Main Menu';
$_['text_slideshow']   = 'Slideshow';
$_['text_promotion']   = 'Promotion';
$_['text_bottom1']   = 'Bottom 1';
$_['text_bottom2']   = 'Bottom 2';
$_['text_bottom3']   = 'Bottom 3';
$_['text_mass_bottom']   = 'Mass Bottom';

$_['text_footer_top']   = 'Footer Top';
$_['text_footer_center']   = 'Footer Center';
$_['text_footer_bottom']   = 'Footer Bottom';
$_['text_create_new'] = 'Создать новый раздел';
$_['text_edit_menu'] = 'Редактировать: %s  (ID:%s)';
$_['all_page']   = 'Все страницы';

$_['text_image_manager']   = 'Менеджер изображений';
// Entry
$_['entry_banner']        = 'Баннер:';
$_['entry_dimension']     = 'Разрешение (Длинны x Ширина) и тип изменения размера:';
$_['entry_layout']        = 'Схема:';
$_['entry_position']      = 'Расположение:';
$_['entry_status']        = 'Статус:';
$_['entry_sort_order']    = 'Порядок сортировки:';
$_['entry_description']    = 'Описание:';
$_['entry_type']    = 'Тип:';
$_['entry_url']    = 'URL:';
$_['entry_category']    = 'Категории:';
$_['entry_product']    = 'Продукт:';
$_['entry_manufacturer']    = 'Производитель:';
$_['entry_information']    = 'Информация:';
$_['entry_html']    = 'HTML:';
$_['entry_parent_id']    = 'ID родителя:';
$_['entry_menuclass']    = 'CSS класс меню:';
$_['entry_showtitle']    = 'Показать заголовок:';
$_['entry_isgroup']    = 'Это группа:';
$_['entry_iscontent']    = 'Это текстовый контент:';
$_['entry_default_column_width']    = 'Стандартная ширина колонки (SpanX): ';
$_['entry_columns']    = 'Колонки';
$_['entry_detail_columns'] = 'Ширина колонок';
$_['entry_sub_menutype'] = 'Тип подменю';
$_['entry_submenu_content'] = 'Содержимое подменю';
$_['text_none'] = 'None';
$_['text_explain_input_html'] = 'If menu content is HTML so all submenu of this menu will not be showed';
$_['text_explain_input_auto'] = 'Type character or word to search';
$_['text_explain_submenu_cols'] = 'Enter detail width of each subcols in values 1->12.<br>Example: col1=3 col3=5';
$_['text_explain_submenu_type'] = 'If the type is Menu, so submenus of this will be showed';
$_['text_browse'] = 'Обзор';
$_['text_clear'] = 'Очистить';
$_['entry_publish'] = 'Опубликовать';


$_['button_save_edit']    = "Сохранить и редактировать";
$_['button_save_new']   = 'Сохранить и создать новое';


//
$_['message_delete'] = "Удалить этот раздел?";
// Error
$_['error_permission']    = 'У вас нет прав для управления меню!';
$_['error_dimension']     = 'Обязательной указать длинну и ширину!';
$_['error_missing_title'] = 'Укажите заголовок меню на всех языках';
$_['text_explain_drapanddrop'] = 'Для сортировки перетащите пункты меню и нажмите кнопку "Обновить"';

$_['Guide'] = 'Помощь';
$_['entry_image'] = 'Изображение';
?>
