<?php
// Heading
$_['heading_title']                 = '<b>Оптовая скидка</b>';
$_['heading_title_head']            = 'Оптовая скидка';
$_['heading_title_mod']             = 'Настройки скидок от суммы заказа';
$_['heading_title_insert']          = 'Добавить скидку';
$_['heading_title_update']          = 'Изменить скидку';

// Text
$_['text_total']                    = 'Общая сумма заказа';
$_['text_success']                  = 'Настройки модуля обновлены!';
$_['button_modulator']              = 'Настройки';


// Entry
$_['entry_status']                  = 'Статус:';
$_['entry_sort_order']              = 'Порядок сортировки:';
$_['entry_customer_group']          = 'Группа оптовиков:';

// Error
$_['error_permission']              = 'У Вас нет прав для управления этим модулем!';

?>