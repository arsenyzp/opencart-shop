<?php
// Heading
$_['heading_title']      = 'ROBOKASSA';

// Text
$_['text_payment']       = 'Payment';
$_['text_success']       = 'Settings updated!';
$_['text_yes']           = 'Yes';
$_['text_no']            = 'No';

// Entry
$_['entry_login']        = 'Login:';
$_['entry_password1']    = 'Password1:';
$_['entry_password2']    = 'Password2:';
$_['entry_test']         = 'Test Mode:';
$_['entry_order_status'] = 'Order status after payment:';
$_['entry_geo_zone']     = 'GEO Zone:';
$_['entry_status']       = 'Status:';
$_['entry_sort_order']   = 'Sort Order:';

// Error
$_['error_permission']   = 'You don't have permission!';
$_['error_login']        = 'Need to enter login!';
$_['error_password1']    = 'Need to enter password1!';
$_['error_password2']    = 'Need to enter password2!';
?>