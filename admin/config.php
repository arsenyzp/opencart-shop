<?php
// HTTP
define('HTTP_SERVER', 'http://magazin.loc:8080/admin/');
define('HTTP_CATALOG', 'http://magazin.loc:8080/');
define('HTTP_IMAGE', 'http://magazin.loc:8080/image/');

// HTTPS
define('HTTPS_SERVER', 'http://magazin.loc:8080/admin/');
define('HTTPS_IMAGE', 'http://magazin.loc:8080/image/');

// DIR
define('DIR_APPLICATION', '/var/www/magazin.loc/admin/');
define('DIR_SYSTEM', '/var/www/magazin.loc/system/');
define('DIR_DATABASE', '/var/www/magazin.loc/system/database/');
define('DIR_LANGUAGE', '/var/www/magazin.loc/admin/language/');
define('DIR_TEMPLATE', '/var/www/magazin.loc/admin/view/template/');
define('DIR_CONFIG', '/var/www/magazin.loc/system/config/');
define('DIR_IMAGE', '/var/www/magazin.loc/image/');
define('DIR_CACHE', '/var/www/magazin.loc/system/cache/');
define('DIR_DOWNLOAD', '/var/www/magazin.loc/download/');
define('DIR_LOGS', '/var/www/magazin.loc/system/logs/');
define('DIR_CATALOG', '/var/www/magazin.loc/catalog/');

// DB
define('DB_DRIVER', 'mysql');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'magazin.loc');
define('DB_PASSWORD', 'magazin.loc');
define('DB_DATABASE', 'magazin.loc');
define('DB_PREFIX', 'oc_');
?>
