<?php
// HTTP
define('HTTP_SERVER', 'http://magazin.loc:8080/');
define('HTTP_IMAGE', 'http://magazin.loc:8080/image/');
define('HTTP_ADMIN', 'http://magazin.loc:8080/admin/');

// HTTPS
define('HTTPS_SERVER', 'http://magazin.loc:8080/');
define('HTTPS_IMAGE', 'http://magazin.loc:8080/image/');

// DIR
define('DIR_APPLICATION', '/var/www/magazin.loc/catalog/');
define('DIR_SYSTEM', '/var/www/magazin.loc/system/');
define('DIR_DATABASE', '/var/www/magazin.loc/system/database/');
define('DIR_LANGUAGE', '/var/www/magazin.loc/catalog/language/');
define('DIR_TEMPLATE', '/var/www/magazin.loc/catalog/view/theme/');
define('DIR_CONFIG', '/var/www/magazin.loc/system/config/');
define('DIR_IMAGE', '/var/www/magazin.loc/image/');
define('DIR_CACHE', '/var/www/magazin.loc/system/cache/');
define('DIR_DOWNLOAD', '/var/www/magazin.loc/download/');
define('DIR_LOGS', '/var/www/magazin.loc/system/logs/');

// DB
define('DB_DRIVER', 'mysql');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'magazin.loc');
define('DB_PASSWORD', 'magazin.loc');
define('DB_DATABASE', 'magazin.loc');
define('DB_PREFIX', 'oc_');
?>
