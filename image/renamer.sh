#!/bin/bash
# Перекодирует рекурсивно в текущем каталоге имена
# файлов и каталогов в транслит.

shopt -s nullglob
for NAME in *
do
TRS=`echo $NAME |tr : _`

TRS=${TRS// /_};

if [[ `file -b "$NAME"` == directory ]]; then
mv -v "$NAME" "$TRS"
cd "$TRS"
"$0"
cd ..
else

mv -v "$NAME" "$TRS"
fi
done

